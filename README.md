# AutoSD Buildbox Cross Compilation Tooling for x86_64-to-aarch64 

This container image provides a development environemnt for those who want to
perform cross compilation tasks for their AutoSD build stack.

## How to build the container

We use the `make` utility to build the container.   The following are the `make` targets that can be used:

```
$ make

Usage:
  make <target>

Help-related tasks
  help                                 Help

Build-related tasks
  build                                Build the container locally (all arches) and print installed test
  amd64                                Build and test the container on amd64
  manifest                             creates the buildah manifest for multi-arch images
  podman-build                         Build amd64
  podman-build-amd64                   build the container in amd64
  test-amd64                           Prints the test of most tools inside the container amd64
  test                                 Tests the container for all the required bits amd64

Run and upload tasks
  run                                  Runs the container interactively
  upload                               Uploads the container to ${UPLOADREGISTRY}/${CONTAINER}
  clean                                Removes any previously built artifact
```

### Build Sequence

There are a few variables that can be set in your shell to build the container:

| VARIABLE    | Description | Default Value |
| -------- | ------- | ------- |
| NAME  | The name of the container.    | autosd-buildbox-x86_64-to-aarch64-cross-tools |
| TAG   | The tag for the container in the Quay repo | latest |
| CONTAINER | Name/Tag name | $(NAME):$(TAG) |
| REGISTRY  | Host for local registry | localhost |
| UPLOADREGISTRY | Target registry to upload container image | quay.io/centos-sig-automotive |

To build the container just execute the following:

```
$ make build 
buildah manifest create "localhost/autosd-buildbox-x86_64-to-aarch64-cross-tools:latest"
ff13c21cd5011b60527bb111859a830e770f46ec93309360268591735620f760
Building the utility container amd64
buildah bud --arch=amd64 --build-arg TARGETARCH=amd64 --build-arg ALTTARGETARCH=x86_64 \
	--build-arg OPTTARGETARCH='' --build-arg EXTRARPMS='' --format docker \
	-f Containerfile -t "autosd-buildbox-x86_64-to-aarch64-cross-tools:latest-amd64"
STEP 1/7: FROM quay.io/centos-sig-automotive/autosd-buildbox:latest
STEP 2/7: LABEL com.github.containers.toolbox="true"       com.redhat.component="$NAME"       name="$NAME"       version="$VERSION"       usage="This image can be used with the toolbox command"       summary="Base image for building AutoSD packages with cross compilation tools"
...
Complete!
68 files removed
COMMIT autosd-buildbox-x86_64-to-aarch64-cross-tools:latest-amd64
Getting image source signatures
Copying blob 87a1067ceb62 skipped: already exists  
Copying blob b2c18eba08ec skipped: already exists  
Copying blob 53dcb01520cb done   | 
Copying config ee34c3f8c9 done   | 
Writing manifest to image destination
--> ee34c3f8c98c
[Warning] one or more build args were not consumed: [ALTTARGETARCH EXTRARPMS OPTTARGETARCH]
Successfully tagged localhost/autosd-buildbox-x86_64-to-aarch64-cross-tools:latest-amd64
ee34c3f8c98c43916b3563242b315a4b4338730f0578ef8199de2c1827eed004
buildah manifest add --arch=amd64 "localhost/autosd-buildbox-x86_64-to-aarch64-cross-tools:latest" "localhost/autosd-buildbox-x86_64-to-aarch64-cross-tools:latest-amd64"
84bcdc27436a8be7d665e46bc03d93a719e9eea18b6810f05596134f9b758575: sha256:ffcddcd2d2406b7b9b2f3f52d9b86b36a6c16c8bfec467dc76d054eb3faaa479
** WIP: Testing linux/amd64
* Arm64 GCC: 
aarch64-redhat-linux-gcc (GCC) 11.4.1 20231218 (Red Hat 11.4.1-3 cross from x86_64)
Copyright (C) 2021 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
```

Once the container build you can upload to the target registry defined by UPLOADREGISTRY variable.

```
$ make upload
Uploading the localhost/autosd-buildbox-x86_64-to-aarch64-cross-tools:latest container to quay.io/claudiol/autosd-buildbox-x86_64-to-aarch64-cross-tools:latest
buildah manifest push --all "localhost/autosd-buildbox-x86_64-to-aarch64-cross-tools:latest" "docker://quay.io/claudiol/autosd-buildbox-x86_64-to-aarch64-cross-tools:latest"
Getting image list signatures
Copying 1 images generated from 1 images in list
Copying image sha256:ffcddcd2d2406b7b9b2f3f52d9b86b36a6c16c8bfec467dc76d054eb3faaa479 (1/1)
Getting image source signatures
Copying blob 53dcb01520cb [================================>-----] 2.2GiCopying blob 53dcb01520cb done   | 
Copying blob 36850128dbaa skipped: already exists  
Copying blob 9e92692856f1 skipped: already exists  
Copying config ee34c3f8c9 done   | 
Writing manifest to image destination
Writing manifest list to image destination
Storing list signatures
```

## License

[MIT](./LICENSE)
